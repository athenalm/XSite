# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
XSite's top-level document processing script.

Syntax: xsite_process.py <document> [<key> <value>...]

Processes <document> using the processor specified by processing instruction, or else the XSlots template specified by the configuration key 'default-template'. Zero or more <key>-<value> pairs may be passed to specify configuration. Results are written to standard output.
"""
from lxml import etree
import sys
import os
import subprocess
import re
from pathlib import Path
import xslots
import xsitelib

def pick_processor(document, config, allow_default=True):
    """
    Determine the processor for a document.
    """
    processor_type = ""
    processor_name = ""

    for pi in document.xpath("//processing-instruction('xsite-processor')"):
        params = xsitelib.pi_params(pi)
        processor_type = params["type"]
        processor_name = params["name"]
        break

    for pi in document.xpath("//processing-instruction('xsite-template')"):
        processor_type = "xslots"
        processor_name = xsitelib.pi_params(pi)["name"]
        break

    if processor_type == "" and allow_default:
        processor_type = "xslots"
        processor_name = config["default-template"]
    
    return (processor_type, processor_name)

def process_document(document, config, params={}):
    """
    Iteratively apply processors, working upwards from the original document to the outermost template. Check each processor's file for PIs specifying the next outer processor. Once the outermost template has been applied, return the document.
    """
    processor_type, processor_name = pick_processor(document, config)

    document = document.getroot()

    xsitelib.load_plugins(config)
    for _, sidecar in xsitelib.plugins["sidecar"].items():
        sidecar_params = xsitelib.document_params(document, params)
        sidecar.process_sidecar(document, config, sidecar_params)

    while processor_type != "":
        if processor_type == "xslots":
            template = etree.parse(str(xsitelib.find_template(config, processor_name)), xslots.PARSER)
            processor_type, processor_name = pick_processor(template, config, allow_default=False) # For next loop; template is clobbered by xslots.apply_template().
            params = xsitelib.document_params(document, params)
            document = xslots.apply_template(template, document, Path(config["resources"]), config, params)
        elif processor_type == "xslt":
            template = etree.parse(str(xsitelib.find_template(config, processor_name)), xslots.PARSER)
            processor_type, processor_name = pick_processor(template, config, allow_default=False) # For next loop.
            params = xsitelib.document_params(document, params)
            sheet = etree.XSLT(template)
            escaped_params = {}
            for k, v in params.items():
                escaped_params[k] = etree.XSLT.strparam(v)
            document = sheet(document, **escaped_params)
        else:
            print("Unknown processor type " + processor_type, file=sys.stderr)

    return document

if __name__ == "__main__":
    document_path = sys.argv[1]

    # Extract configuration from command-line arguments.
    config = {}

    key = None
    for arg in sys.argv[2:]:
        if key is None:
            key = arg
        else:
            config[key] = arg
            key = None

    document = etree.parse(document_path, xslots.PARSER)
    source_file = str(Path(document_path).relative_to(config["documents"]))
    output_file = xsitelib.document_output_path(config, source_file)
    document = process_document(document, config, params={"source-file": source_file, "output-file": output_file, "document-uri": xsitelib.document_uri(config, output_file)})

    out = os.fdopen(sys.stdout.fileno(), 'wb')
    xslots.write_output(document, out)
