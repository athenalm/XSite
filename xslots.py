# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
The XSlots interpreter.

Syntax: xslots.py <template> <content>[<key> <value>...]

Processes <template>, using <content> as the main content document. Zero or more <key>-<value> pairs may be passed to specify configuration. Results are written to standard output.
"""
from lxml import etree
import sys
import os
import re
from io import BytesIO, SEEK_END
from pathlib import Path
import importlib
import xsite_process
import xsitelib

NS = {"sl": "https://www.alm.website/misc/specs/xslots"}
PARAM_PI_REGEX = re.compile("([^\s]+)=\"(.+?)\"|([^\s]+)=(.+?)")
PARSER = etree.XMLParser(remove_blank_text=True)

def insert_element(slot, content):
    """
    Insert the given content element where the slot is.
    """
    if content.tag == "{https://www.alm.website/misc/specs/xslots}fragment":
        for node in content.xpath("./*"): # Insert each node of the fragment.
            slot.addprevious(node)
        slot.getparent().remove(slot)
    else:
        slot.getparent().replace(slot, content)

def insert_text(slot, text):
    """
    Insert the given text where the slot is.
    """
    if slot.getprevious() is not None:
        slot.getprevious().tail = (slot.getprevious().tail or "") + text + (slot.tail or "")
    else:
        slot.getparent().text = (slot.getparent().text or "") + text + (slot.tail or "")
    slot.getparent().remove(slot)

def apply_template(template, content, resources, config, params=None):
    """
    Process the template (clobbering it!), using the given content, retrieiving resources from the given path, and using the given configuration, and return the resulting document. Parameters are extracted from the document if not specified explicitly.
    """
    template = next(template.getroot().iterchildren())

    if params == None:
        params = xsitelib.document_params(content)

    # Fill <sl:content> elements.
    slots = template.xpath("//sl:content", namespaces=NS)
    for slot in slots:
        if len(slot.keys()) == 0: # No attributes, main content.
            insert_element(slot, content)
        else: # Parameter, config, or resource.
            if slot.get("param") is not None:
                insert_text(slot, params.get(slot.get("param")) or "")
            elif slot.get("config") is not None:
                insert_text(slot, config.get(slot.get("config")) or "")
            elif slot.get("resource") is not None: # Literal inclusion of resource.
                with (resources / slot.get("resource")).open("r") as f:
                    insert_text(slot, f.read())
            elif slot.get("document") is not None: # Inclusion of processed document.
                document = etree.parse(str(Path(config["documents"]) / slot.get("document")), PARSER)
                document = xsite_process.process_document(document, config, params)
                insert_element(slot, document.getroot())
            elif slot.get("dataset") is not None and slot.get("datatype") is not None: # Inclusion of XML generated from data set.
                try:
                    insert_element(slot, xsitelib.plugins["datasets"][slot.get("datatype")].get_dataset(resources, slot.get("dataset")))
                except KeyError:
                    print("Warning: Unrecognized dataset type " + slot.get("datatype") + ", ignoring.", file=sys.stderr)
                    slot.getparent().remove(slot)
            else:
                print("Warning: Unrecognized content slot " + etree.tostring(slot).decode("utf-8") + ", ignoring.", file=sys.stderr)
                slot.getparent().remove(slot)

    # Apply <sl:attrib> elements.
    slots = template.xpath("//sl:attrib", namespaces=NS)
    for slot in slots:
        if slot.get("param") is not None:
            slot.getparent().set(slot.get("name"), params.get(slot.get("param")) or "")
        elif slot.get("config") is not None:
            slot.getparent().set(slot.get("name"), config.get(slot.get("config")) or "")
        else:
            print("Warning: Unrecognized attrib slot " + etree.tostring(slot).decode("utf-8") + ", ignoring.", file=sys.stderr)
        slot.getparent().remove(slot)

    # Apply <sl:xslt> elements.
    slots = template.xpath("//sl:xslt", namespaces=NS)
    for slot in slots:
        if len(slot) == 1:
            sheet = etree.XSLT(etree.parse(str(xsitelib.find_template(config, slot.get("name"))), PARSER))
            escaped_params = {}
            for k, v in params.items():
                escaped_params[k] = etree.XSLT.strparam(v)
            for k, v in config.items():
                escaped_params["conf-" + k] = etree.XSLT.strparam(v)
            insert_element(slot, sheet(slot[0], **escaped_params).getroot())
        else:
            print("Warning: Incorrect number of children to XSLT slot " + etree.tostring(slot).decode("utf-8") + ", ignoring.", file=sys.stderr)
            slot.getparent().remove(slot)

    return template

def write_output(document, out, **kwargs):
    """
    Commit whatever horrible hacks are necessary to get around weirdness in lxml, then write the document to the file-like object out. Additional keyword arguments will be passed to etree ElementTree.write().
    """
    buf = BytesIO()
    etree.ElementTree(document).write(buf, encoding="utf-8", xml_declaration=True, pretty_print=True, inclusive_ns_prefixes=[], exclusive=True, **kwargs)

    # FIXME: Awful hack to truncate away the last nonblank line if it's a template end tag. Some versions of libxml2 or lxml or something will leave an extra dangling end tag, which is obviously wrong and invalid.
    buf.seek(0)
    lines = buf.readlines()
    line_num = -1
    while lines[line_num] == b"\n":
        line_num -= 1
    last_line = lines[line_num]
    del lines

    if b"template>" in last_line:
        buf.seek(-len(last_line) + line_num + 1, SEEK_END) # Truncate right up to the newline ending the last non-end-tag, nonblank line
        buf.truncate()
    out.write(buf.getvalue())

if __name__ == "__main__":
    template_path = sys.argv[1]
    content_path = sys.argv[2]

    # Extract configuration from command-line arguments.
    config = {}

    key = None
    for arg in sys.argv[3:]:
        if key is None:
            key = arg
        else:
            config[key] = arg
            key = None

    result = apply_template(etree.parse(template_path, PARSER), etree.parse(content_path, PARSER), Path(config["resources"]), config)

    out = os.fdopen(sys.stdout.fileno(), 'wb')
    write_output(result, out)
