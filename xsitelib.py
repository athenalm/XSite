from pathlib import Path, PurePath, PurePosixPath
import re
import sys
import importlib
import pkgutil

PARAM_PI_REGEX = re.compile("([^\s]+)=\"(.+?)\"|([^\s]+)=(.+?)")

def pi_params(pi):
    """
    Parse pseudo-parameters from a processing instruction.
    """
    params = {}
    for match in PARAM_PI_REGEX.finditer(pi.text):
        if match[1] is not None:
            params[match[1]] = match[2]
        else:
            params[match[3]] = match[4]
    return params

def document_params(document, merge={}):
    """
    Extract parameters from <?xsite-params?> processing instructions in the given document. Parameters specified in merge override those extracted from the document.
    """
    params = {}
    for pi in document.xpath("//processing-instruction('xsite-params')"):
        params.update(pi_params(pi))
    params.update(merge)
    return params

def document_output_path(config, path):
    """
    Get the output path (relative to the output directory) corresponding to a document input path (relative to the documents directory).

    Currently, all output paths are the same as input paths. This may, however, change some day.
    """
    return path

def document_uri(config, path):
    """
    Get the URI corresponding to an output document path (relative to the output directory).

    If you have an input path, you can use document_output_path() to get the output path.
    """
    path = PurePosixPath(PurePath(path).as_posix()) # Normalize to POSIX-like path in case of Windows; POSIX-like paths are closest to URI paths.
    if config.get("uri-indexes") != "true" and path.stem == "index":
        if len(path.parts) == 1: # Prevent /index.html from becoming /./ (which isn't technically wrong but is definitely not the canonical path).
            path_str = ""
        else:
            path_str = str(path.parent) + "/"
    elif config.get("uri-extensions") != "true":
        path_str = str(path.with_suffix(""))
    else:
        path_str = str(path)
    return config["site-base-url"] + "/" + path_str

plugins = {
    "datasets": {},
    "sidecar": {}
}

def load_plugins(config):
    for path in config["plugins"].split(":"):
        sys.path.append(path)
    for info in pkgutil.iter_modules(config["plugins"].split(":")):
        parts = info.name.split("_")
        if len(parts) > 1:
            plugins[parts[1]][parts[0]] = importlib.import_module(info.name)

def find_template(config, name):
    for path in config["templates"].split(":"):
        filename = (Path(path) / name)
        if filename.is_file():
            return filename
